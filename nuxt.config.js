export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Nuxt.js - Vuetify admin template!',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Nuxt.js + Vuetify.js based admin template! with premade components for building a SAAS application, Portfolio, Ecommerce site etc.',
      },
      {
        hid: 'title',
        name: 'title',
        content: 'Nuxt.js + Vuetify.js based admin template!',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'Nuxt.js, Vuetify.js, Admin template, Vue.js, Ecommerce template, Portfolio template',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  // loaders
  // loading: '~/components/PageLoading.vue',
  loading: {
    color: '#ff5c93',
    height: '4px',
  },

  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'black',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['element-ui/lib/theme-chalk/index.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '~/plugins/VueApexCharts.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components',
      '~/components/cards',
      '~/components/apex-charts',
      '~/components/chartjs',
    ],
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxtjs/vuetify'],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios'],

  axios: {
    baseURL: process.env.NLP_SVC_BASE_URL, // 'https://nlp.insightstream.dev', // 'http://localhost:8000', // Used as fallback if no runtime config is provided
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
    // vendor: ['vue-apexchart'],
  },
  // server config
  server: {
    port: 3001,
    host: '0.0.0.0',
  },
  target: 'static',
  ssr: false,
  generate: {
    fallback: true,
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    optionsPath: './vuetify.options.js',
  },
}
