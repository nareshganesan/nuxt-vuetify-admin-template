# Nuxt Vuetify Admin Template

Nuxt.js + Vuetify based admin template

## Dependencies

```bash
# vs code dependencies
npm i -D eslint eslint-config-prettier eslint-plugin-prettier prettier

# install nuxt
npm install nuxt
```

## Create Nuxt app template

```bash
# create nuxt app
npm init nuxt-app <project-name>
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
