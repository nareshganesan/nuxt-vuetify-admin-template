import colors from 'vuetify/es5/util/colors'

export default {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#536dfe',
        secondary: '#ff5c93',
        accent: '#82b1ff',
        success: '#3cd4a0',
        info: '#9013fe',
        warning: '#ffc260',
        error: '#df1368',
        background: colors.shades.black,
      },
      light: {
        primary: '#536dfe',
        secondary: '#ff5c93',
        accent: '#82b1ff',
        success: '#3cd4a0',
        info: '#9013fe',
        warning: '#ffc260',
        error: '#df1368',
        background: colors.shades.white,
      },
    },
  },
}
